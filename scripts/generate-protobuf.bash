#!/usr/bin/env bash

protoc \
    --plugin=protoc-gen-nanopb=nanopb/generator/protoc-gen-nanopb \
    --proto_path=source \
    --proto_path=nanopb/generator/proto \
    --nanopb_out=build \
    --csharp_out=build \
    source/master.proto
